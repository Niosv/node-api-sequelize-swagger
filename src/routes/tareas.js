import { Router } from "express";
import {
  getAllTareas,
  getTareaById,
  addTarea,
  modTarea,
  delTarea,
  getTareaByProjectId,
  checkTarea,
} from "../controllers/tareas.js";

const router = Router();

router.get("/tarea", getAllTareas);

router.get("/tarea/:id", getTareaById);

router.get("/tareaByProject/:id", getTareaByProjectId);

router.post("/tarea", addTarea);

router.put("/tarea/:id", modTarea);

router.put("/tareaCheck/:id", checkTarea);

router.delete("/tarea/:id", delTarea);

export default router;
