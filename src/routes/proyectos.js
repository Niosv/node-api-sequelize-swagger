import { Router } from "express";
import {
  getAllProyectos,
  getProyectoById,
  addProyecto,
  modProyecto,
  delProyecto,
} from "../controllers/proyectos.js";

const router = Router();

router.get("/proyecto", getAllProyectos);

router.get("/proyecto/:id", getProyectoById);

router.post("/proyecto", addProyecto);

router.put("/proyecto/:id", modProyecto);

router.delete("/proyecto/:id", delProyecto);

export default router;
