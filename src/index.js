import express from "express";
import morgan from "morgan";
import { sequelize } from "./database/database.js";
import proyectoRoutes from "./routes/proyectos.js";
import tareasRoutes from "./routes/tareas.js";

//TODO: Se peude unificar en un solo archivo
// import "./models/cargo.js";
// import "./models/proyecto.js";
// import "./models/tarea.js";
// import "./models/usuario.js";

const app = express();

app.use(express.json());
app.use(morgan("dev"));

//routes
app.use(proyectoRoutes);
app.use(tareasRoutes);

const main = async () => {
  try {
    // await sequelize.sync({ force: true });
    console.log("Se estableció conexión con la base de datos");
    app.listen(3000, () => {
      console.log("Escuchando en el puerto 3000");
    });
  } catch (error) {
    console.error("No se pude conectar a la base de datos: ", error);
  }
};

main();
