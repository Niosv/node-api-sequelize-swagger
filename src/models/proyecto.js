import { sequelize } from "../database/database.js";
import { DataTypes } from "sequelize";
import { Tarea } from "./tarea.js";

export const Proyecto = sequelize.define("proyecto", {
  id: {
    type: DataTypes.BIGINT,
    primaryKey: true,
    autoIncrement: true,
  },
  nombre: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  descripcion: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  duracion: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
});

//Relacion de las tablas
Proyecto.hasMany(Tarea, {
  foreingKey: "proyectoId",
  sourceKey: "id",
});

Tarea.belongsTo(Proyecto, {
  foreingKey: "proyectoId",
  targetId: "id",
});
