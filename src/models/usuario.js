import { sequelize } from "../database/database.js";
import { DataTypes } from "sequelize";
import { Proyecto } from "./proyecto.js";
import { Tarea } from "./tarea.js";

export const Usuario = sequelize.define("usuario", {
  id: {
    type: DataTypes.BIGINT,
    primaryKey: true,
    autoIncrement: true,
  },
  nombre: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  apellido: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  correo: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
  },
  passowrd: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

Usuario.hasMany(Proyecto, {
  foreignKey: "usuarioId",
  sourceKey: "id",
});

Usuario.hasMany(Tarea, {
  foreignKey: "usuarioId",
  sourceKey: "id",
});

Proyecto.belongsTo(Usuario, {
  foreignKey: "usuarioId",
  targetId: "id",
});

Tarea.belongsTo(Usuario, {
  foreignKey: "usuarioId",
  targetId: "id",
});
