import { sequelize } from "../database/database.js";
import { DataTypes } from "sequelize";
import { Usuario } from "./usuario.js";

export const Cargo = sequelize.define("cargo", {
  id: {
    type: DataTypes.BIGINT,
    primaryKey: true,
    autoIncrement: true,
  },
  nombre: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

Cargo.hasMany(Usuario, {
  foreignKey: "cargoId",
  sourceKey: "id",
});

Usuario.belongsTo(Cargo, {
  foreignKey: "cargoId",
  targetId: "id",
});
