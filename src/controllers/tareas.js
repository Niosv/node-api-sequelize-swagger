import { Tarea } from "../models/tarea.js";

export const getAllTareas = async (req, res) => {
  try {
    const tareas = await Tarea.findAll();
    if (tareas.length === 0) {
      return res.status(404).json({ message: "No se encontraron tareas" });
    }

    res.status(200).json(tareas);
  } catch (error) {
    res.status(500).json({ message: "Error en el servidor ", error });
  }
};

export const getTareaById = async (req, res) => {
  try {
    const { id } = req.params;
    const tarea = await Tarea.findAll({
      where: {
        id: id,
      },
    });
    if (tarea.length === 0) {
      return res.status(404).json({ message: "No se encontró esta tarea" });
    }

    res.status(200).json(tarea[0]);
  } catch (error) {
    res.status(500).json({ message: "Error en el servidor ", error });
  }
};
export const getTareaByProjectId = async (req, res) => {
  try {
    const { id } = req.params;
    const tareas = await Tarea.findAll({
      where: {
        proyectoId: id,
      },
    });
    if (tareas.length === 0) {
      return res
        .status(404)
        .json({ message: "No existen tareas para este proyecto" });
    }

    res.status(200).json(tareas);
  } catch (error) {
    res.status(500).json({ message: "Error en el servidor ", error });
  }
};

export const addTarea = async (req, res) => {
  try {
    const tarea = req.body;
    await Tarea.create(tarea);
    res.status(200).json({ message: "Se creó una tarea" });
  } catch (error) {
    res.status(500).json({ message: "Error en el servidor ", error });
  }
};

export const modTarea = async (req, res) => {
  try {
    const { id } = req.params;
    const tarea = req.body;
    await Tarea.update(tarea, { where: { id: id } });
    res.status(200).json({ message: "Tarea actualizada" });
  } catch (error) {
    res.status(500).json({ message: "Error en el servidor ", error });
  }
};

export const checkTarea = async (req, res) => {
  try {
    const { id } = req.params;
    await Tarea.update({ estado: true }, { where: { id: id } });
    res.status(200).json({ message: "Tarea lista" });
  } catch (error) {
    res.status(500).json({ message: "Error en el servidor ", error });
  }
};

export const delTarea = async (req, res) => {
  try {
    const { id } = req.params;
    await Tarea.destroy({ where: { id: id } });
    res.status(200).json({ message: "Tarea eliminada" });
  } catch (error) {
    res.status(500).json({ message: "Error en el servidor ", error });
  }
};
