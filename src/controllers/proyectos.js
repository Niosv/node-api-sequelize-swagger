import { Proyecto } from "../models/proyecto.js";

export const getAllProyectos = async (req, res) => {
  try {
    const proyectos = await Proyecto.findAll();
    if (proyectos.length === 0) {
      return res
        .status(404)
        .json({ message: "No existen proyectos registrados" });
    }
    res.status(200).json(proyectos);
  } catch (error) {
    res.status(500).json({ message: "No se encontraron proyectos" });
  }
};

export const getProyectoById = async (req, res) => {
  const { id } = req.params;
  const proyecto = await Proyecto.findAll({
    where: {
      id: id,
    },
  });

  if (proyecto.length === 0) {
    return res.status(404).json({ message: "No existe este proyecto" });
  }
  res.status(200).json(proyecto[0]);
  try {
  } catch (error) {
    res.status(500).json({ message: "No se encontraron proyectos" });
  }
};

export const addProyecto = async (req, res) => {
  try {
    const proyecto = req.body;
    await Proyecto.create(proyecto);
    res.status(200).json({ message: "Proyecto creado" });
  } catch (error) {
    res
      .status(500)
      .json({ message: "No se pudo agregar el proyecto", error: error });
  }
};

export const modProyecto = async (req, res) => {
  try {
    const { id } = req.params;
    const proyecto = req.body;
    await Proyecto.update(proyecto, {
      where: {
        id: id,
      },
    });
    res.status(200).json({ message: "Proyecto modificado" });
  } catch (error) {
    res.status(500).json({ message: "No se encontraron proyectos" });
  }
};

export const delProyecto = async (req, res) => {
  try {
    const { id } = req.params;
    await Proyecto.destroy({
      where: {
        id: id,
      },
    });
    res.status(200).json({ message: "Se eliminó el proyecto" });
  } catch (error) {
    res.status(500).json({ message: "No se encontraron proyectos" });
  }
};
